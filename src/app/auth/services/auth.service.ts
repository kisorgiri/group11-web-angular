import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from 'src/app/shared/services/base.service';


// @Injectable()
export class AuthService extends BaseService {
    constructor(private http: HttpClient) {
        super();
        this.setUrl('auth');
    }

    login(data: User) {
        console.log('thi.url >>', this.url);
        return this.http.post(this.url + 'login', data, this.getOptions());
    }

    register(data: User) {
        return this.http.post(this.url + 'register', data, this.getOptions())
    }

    forgotPassword(data) {
        return this.http.post(this.url + 'forgotPassword', data, this.getOptions())

    }

    resetPassword(data) {
        return this.http.post(this.url + 'resetPassword/' + data.token, data, this.getOptions())

    }

}


// summarize
// services injectable decorator defined class
//services should be placed in providers section of module
// services should be injected before using( constructor)
// mostly services are used for making API call and handling most common methods
// httpClientModule is used to communicate in http protocal, and injected in imports
// httpclient will alow us to communicate using http method
// each method has its own syntax with required arguments 
// eg post require url, body, headers
// making an http call require one of  result handling mechanism to be applied
// http client module always return observable so result handling mechanism should be observabale


// CORS
// although FE send request to BE BE wont process the request if it is from different oriign

// CORS enable (easy way accept all request)// 
// CORS are maintained in BE

// status code are very important when communicating using http protocol
//


