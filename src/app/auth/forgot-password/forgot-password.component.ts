import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/message.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { User } from '../models/user.model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  user;
  submitting: boolean = false;
  constructor(
    public msgService: MsgService,
    public authService: AuthService,
    public router: Router,
  ) {
    this.user = new User({});
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.authService.forgotPassword(this.user)
      .subscribe(data => {
        this.submitting = false;
        this.msgService.showSuccess("password reset link sent to email please check your inbox");
        this.router.navigate(['/auth/login']);
      },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        })
  }

}
