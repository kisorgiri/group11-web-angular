import { Component } from '@angular/core';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';
import { MsgService } from 'src/app/shared/services/message.service';


@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    public user: User;
    public submitting: boolean = false;
    public loginByEmail: boolean = false;
    constructor(
        public router: Router,
        public authService: AuthService, // j j kura injectable decorator use gareko cha tyo inject garnu parcha,
        public msgService: MsgService
    ) {
        this.user = new User({});//
        console.log('get token ', localStorage.getItem('token'));
        console.log('get user >>>', JSON.parse(localStorage.getItem('user')));
    }

    getNote() {
        return new Promise((resolve, reject) => {
            setInterval(() => {
                console.log('excuted block');
                resolve('hi');
            }, 2000);
        });
    }





    login() {
        // this.getNote()
        //     .then((data) => {
        //         console.log('success in promise >>', data);
        //     })
        //     .catch((err) => {
        //         console.log('error is ', err);
        //     });
        // let riyaz = this.authService.watchGOT()
        //     .subscribe(
        //         (data) => {
        //             console.log('success in riyaz >>>', data);
        //         },
        //         (error) => {
        //             console.log('error in observable >>>', error);
        //         },
        //         () => {
        //             console.log('completed >>>')
        //         }
        //     )
        // let kishmat = this.authService.watchGOT()
        //     .subscribe(
        //         (data) => {
        //             console.log('success for kishmat', data);
        //             if (data.episode == 3) {
        //                 kishmat.unsubscribe();
        //             }
        //         },
        //         (error) => {
        //             console.log('error >>>>>', error);
        //         },
        //         () => {
        //             console.log('complete in kishmat >>');
        //         }
        //     )

        this.submitting = true;
        this.authService.login(this.user)
            .subscribe(
                (data: any) => {
                    console.log('dat ais >>', data);
                    this.msgService.showSuccess(`welcome ${data.user.username}`);
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('user', JSON.stringify(data.user));
                    this.router.navigate(['user/dashboard'])

                    this.submitting = false;
                },
                (err) => {
                    this.msgService.showError(err);
                    console.log('failure in observable >>', err);
                    this.submitting = false;
                }
            )
        // expectation would be success or failure



    }

    changeLoginMethod() {
        this.loginByEmail = !this.loginByEmail;
    }
}

// data binding
// communication of data between view and controller

// types of data binding
// event binding (click,change,hove,doubleclick) syntax ()
// property binding // disabled/ hidden  // syntax []
//  two way data binding // ==> synchronization of data between views and controller
// if(changes in views reflect in controller) and changes in controller reflects in views


