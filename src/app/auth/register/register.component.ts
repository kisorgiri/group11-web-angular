import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  submitting: boolean = false;
  user;
  username: string;
  password: string;
  constructor(
    public authService: AuthService,
    public msgService: MsgService,
    public router: Router,
  ) {
    this.user = new User({});
  }

  ngOnInit() {

  }

  register() {
    this.submitting = true;
    this.authService.register(this.user)
      .subscribe(
        (data: any) => {
          this.msgService.showSuccess('Regisration successfull');
          this.router.navigate(['/auth/login']);
        },
        (error) => {
          this.submitting = false;
          this.msgService.showError(error);
        }
      )
  }

}
