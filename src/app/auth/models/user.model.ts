export class User {
    username: string;
    password: string;
    name: string;
    phoneNumber: number;
    email: string;
    gender: string;
    address: string;
    id: string;
    dob: string;


    constructor(details: any) {
        this.username = details.username || '';
        this.password = details.password || '';
        this.name = details.name || '';
        this.phoneNumber = details.phoneNumber || 0;
        this.email = details.email || '';
        this.gender = details.gender || '';
        this.address = details.address || '';
        this.id = details.id || '';
        this.dob = details.dob || '';
    }

    setUser() {
        return 'hello';
    }
}

