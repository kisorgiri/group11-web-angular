import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/message.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../models/user.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  submitting: boolean = false;
  user;
  constructor(
    public authService: AuthService,
    public msgService: MsgService,
    public router: Router,
    public activeRouter: ActivatedRoute,
  ) {
    this.user = new User({});
  }

  ngOnInit() {
    this.user.token = this.activeRouter.snapshot.params['token'];
  }

  submit() {
    this.submitting = true;
    this.authService.resetPassword(this.user)
      .subscribe(data => {
        this.submitting = false;
        this.msgService.showSuccess('Password reset successfull please login');
        this.router.navigate(['/auth/login']);
      },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        })
  }

}
