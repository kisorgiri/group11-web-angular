import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';

export class BaseService {

    url;
    constructor() {
        this.url = environment.BaseUrl;
    }

    /**
     * 
     * @param url 
     * @example
     * 'about'
     * @returns string
     * localhost:4040/about/
     */
    setUrl(url) {
        return this.url += url + '/';
    }


    getOptions() {
        // if (!token) {
        //     return {
        //         headers: new HttpHeaders({
        //             'Content-Type': 'application/json'
        //         }),
        //     };
        // } else {
        //     return {
        //         headers: new HttpHeaders({
        //             'Content-Type': 'application/json',
        //             'x-access-token': localStorage.getItem('token'),
        //         }),
        //     }
        // }
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
        };
    }

    public getOptionsWithToken() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('token'),
            }),
        }
    }

}