import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class UplaodService {
    url;
    constructor() {
        this.url = environment.BaseUrl;
    }
    upload(data, file, method, url) {
        return Observable.create((observer) => {
            //uploading process
            console.log(file);
            let xhr = new XMLHttpRequest(); //
            let formData = new FormData();

            if (file) {
                formData.append('image', file[0], file[0].name);
            }

            for (let key in data) {
                formData.append(key, data[key]);
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            let url;
            if (method === 'PUT') {
                url = `${this.url}${data._id}?token=${localStorage.getItem('token')}`;
            }
            else if (method === 'POST') {
                url = this.url + '?token=' + localStorage.getItem("token")
            }
            else {
                url = '';
            }
            xhr.open(method, url, true);
            xhr.send(formData);
        })

    }
}