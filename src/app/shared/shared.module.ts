import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FooterComponent } from './footer/footer.component';
import { MsgService } from './services/message.service';
import { LoaderComponent } from './loader/loader.component';
import { SocketService } from './services/socket.service';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    LoaderComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [MsgService, SocketService],
  exports: [HeaderComponent, FooterComponent, PageNotFoundComponent, LoaderComponent]
})
export class SharedModule { }
