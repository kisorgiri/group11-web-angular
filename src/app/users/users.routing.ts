import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ChatComponent } from './chat/chat.component';
const userRoute: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent
    }, {
        path: 'profile',
        component: ProfileComponent
    },
    {
        path: 'chat',
        component: ChatComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(userRoute)],
    exports: [RouterModule],
})
export class UserRoutingModule {

}