import { Component, OnInit } from '@angular/core';
import { SocketService } from 'src/app/shared/services/socket.service';
import { MsgService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent {
  msg;
  messages = [];
  users = [];
  user;
  msgBody;
  constructor(public socketService: SocketService,
    public msgService: MsgService) {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.runSocket();
    this.msgBody = {
      msg: '',
      senderId: '',
      receiverId: '',
      user: '',
      time: ''
    }
  }

  sendMsg() {
    if (!this.msgBody.receiverId) {
      this.msgService.showInfo("please select user to continue");
      return;
    }
    this.users.forEach((item) => {
      if (this.user.username === item.name) {
        this.msgBody.senderId = item.id;
      }
    });
    this.msgBody.user = this.user.username;
    this.msgBody.time = new Date();
    this.socketService.socket.emit('new-msg', this.msgBody);
    this.msgBody.msg = '';

  }

  runSocket() {
    this.socketService.socket.emit('new-user', this.user.username);
    this.socketService.socket.on('reply-to-msg', (data) => {
      console.log('data in new event >', data);
      this.msgBody.receiverId = data.senderId;
      this.messages.push(data);
    });
    this.socketService.socket.on('reply-msg', (data) => {
      console.log('data in new event >', data);
      this.messages.push(data);
    });
    this.socketService.socket.on('users', (data) => {
      this.users = data;
    });
  }

  setUser(user) {
    this.msgBody.receiverId = user.id;
  }

  focusedIn(bool) {
    console.log('bool >>', bool);
  }

  // runS

}
