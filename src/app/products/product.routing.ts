import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { AddProductComponent } from './add-product/add-product.component';
import { ListProductComponent } from './list-product/list-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { SearchProductComponent } from './search-product/search-product.component';
const productRoutes: Routes = [
    {
        path: 'add',
        component: AddProductComponent
    },
    {
        path: 'list',
        component: ListProductComponent
    },
    {
        path: 'edit/:id',
        component: EditProductComponent
    },
    {
        path: 'search',
        component: SearchProductComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(productRoutes)],
    exports: [RouterModule]
})

export class ProductRoutingModule {

}