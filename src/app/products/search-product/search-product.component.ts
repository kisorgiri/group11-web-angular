import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/message.service';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';
import { Product } from '../models/product.model';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.css']
})
export class SearchProductComponent implements OnInit {
  product;
  products = [];
  submitting: boolean = false;
  result: boolean = false;
  allProducts = [];
  categories = [];
  names = [];
  showName: boolean = false;
  showMultipleDate: boolean = false;
  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router,

  ) {
    this.product = new Product({});
    this.product.category = '';

  }

  ngOnInit() {
    this.productService.get().subscribe(
      (data: any) => {
        console.log('data >>>', data);
        this.allProducts = data;
        data.forEach((item) => {
          if (this.categories.indexOf(item.category) === -1) {
            this.categories.push(item.category);
          }
        });
      }, error => {
        this.msgService.showError(error);
      }
    )
  }

  search() {
    if (!this.product.toDate) {
      this.product.toDate = this.product.fromDate;
    }
    this.submitting = true;
    this.productService.search(this.product)
      .subscribe((data: any) => {
        this.submitting = false;

        if (data.length) {
          this.products = data;
          this.result = true;
          console.log('data is >>', data);
        } else {
          this.msgService.showInfo('No any products matched your search query');
        }
      },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        })
  }

  categoryChanged(cat) {
    // console.log(cat);
    this.names = this.allProducts.filter((item) => {
      if (item.category == cat) {
        return true;
      }
    })
    this.showName = true;
    this.product.name = '';


  }

  chanegMultipleDate() {
    if (this.showMultipleDate) {
      this.product.toDate = null;
    }
    this.showMultipleDate = !this.showMultipleDate;
    
  }

}
