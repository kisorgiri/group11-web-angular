import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/message.service';
import { ProductService } from '../services/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  productId;
  product;
  loading: boolean = false;
  submitting: boolean = false;
  uploadArray = [];
  imgUrl: string;
  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router,
    public activeRoute: ActivatedRoute
  ) {
    this.imgUrl = environment.ImgUrl;
  }

  ngOnInit() {
    this.loading = true;
    this.productId = this.activeRoute.snapshot.params['id'];
    console.log('product id is >>>', this.productId);
    this.productService.getById(this.productId)
      .subscribe(
        (data: any) => {
          this.loading = false;
          this.product = data;
          this.product.tags = data.tags.join(',');
        },
        error => {
          this.loading = false;
          this.msgService.showError(error);
        }
      )
  }

  update() {
    this.submitting = true;
    this.productService.upload(this.product, this.uploadArray,'PUT')
      .subscribe(
        data => {
          this.msgService.showSuccess('Product Updated successfully');
          this.router.navigate(['/product/list']);
        },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        });
  }

  fileChanged(ev) {
    this.uploadArray = ev.target.files;
  }

}
