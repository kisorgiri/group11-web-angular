import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/message.service';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';
import { Product } from '../models/product.model';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  product;
  submitting: boolean = false;
  uploadArray = [];
  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router,
  ) {
    this.product = new Product({});
  }

  ngOnInit() {
  }

  add() {
    this.submitting = true;
    this.productService.add(this.product)
      .subscribe(data => {
        this.msgService.showInfo('Product added successfully');
        this.router.navigate(['/product/list']);
      },
        error => {
          this.msgService.showError(error);
        })
  }

  fileChanged(ev) {
    this.uploadArray = ev.target.files; // files
  }

  upload() {
    this.submitting = true;
    this.productService.upload(this.product, this.uploadArray, "POST")
      .subscribe(data => {
        this.msgService.showInfo('Product added successfully');
        this.router.navigate(['/product/list']);
      },
        error => {
          this.msgService.showError(error);
        })
  }

}
