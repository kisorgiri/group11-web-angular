import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';
import { BaseService } from 'src/app/shared/services/base.service';
import { catchError, map } from 'rxjs/operators';

@Injectable()

export class ProductService extends BaseService {
    constructor(public http: HttpClient) {
        super();
        this.setUrl('product');
    }

    get() {
        return this.http.get(this.url, this.getOptionsWithToken());
    }
    getById(id) {
        return this.http.get(this.url + '/' + id, this.getOptionsWithToken());

    }
    add(data: Product) {
        return this.http.post(this.url, data, this.getOptionsWithToken());
    }

    update(data: any) {
        return this.http.put(this.url + '/' + data._id, data, this.getOptionsWithToken());

    }
    remove(id: string) {
        return this.http.delete(this.url + '/' + id, this.getOptionsWithToken());

    }
    search(data) {
        return this.http.post(this.url + 'search', data, this.getOptionsWithToken());
    }

    upload(data, file, method) {
        return Observable.create((observer) => {
            //uploading process
            console.log(file);
            let xhr = new XMLHttpRequest(); //
            let formData = new FormData();

            if (file) {
                formData.append('image', file[0], file[0].name);
            }

            for (let key in data) {
                formData.append(key, data[key]);
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            let url;
            if (method === 'PUT') {
                url = `${this.url}${data._id}?token=${localStorage.getItem('token')}`;
            }
            else if (method === 'POST') {
                url = this.url + '?token=' + localStorage.getItem("token")
            }
            else {
                url = '';
            }
            xhr.open(method, url, true);
            xhr.send(formData);
        })

    }
}