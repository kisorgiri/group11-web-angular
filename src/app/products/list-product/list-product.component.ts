import { Component, OnInit, Input } from '@angular/core';
import { MsgService } from 'src/app/shared/services/message.service';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products = [];
  loading: boolean = false;
  imgUrl: string;

  @Input() data: any;

  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router,
  ) {
    this.imgUrl = environment.ImgUrl;
  }

  ngOnInit() {
    console.log('data is >>', this.data);
    if (!this.data) {
      this.loading = true;
      this.productService.get().subscribe(
        (data: any) => {
          // data.forEach((item) => {
          //   item.tags = item.tags.join(',');
          // })
          this.products = data;
          this.loading = false;
        }, error => {
          this.loading = false;
          this.msgService.showError(error);
        }
      );
    } else {
      this.products = this.data;
    }

  }

  editProduct(id) {
    this.router.navigate(['/product/edit/' + id]);
  }

  removeProduct(id, i) {
    let a = confirm('Are you sure to remove?')
    if (a) {
      this.productService.remove(id).subscribe(
        data => {
          this.msgService.showSuccess('Product deleted');
          this.products.splice(i, 1);
        },
        error => {
          this.msgService.showError(error);
        });
    }
  }

}
