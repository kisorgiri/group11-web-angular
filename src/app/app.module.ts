import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

// routing configuration
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';


// summarize
// routing  routing module 
// routing configuration
//  router-outlet directve to use as sofa set
// routerLink is used for navigation


@NgModule({ // decorator
  declarations: [ // properites of meta data
    // / components/pipes/directives are place in declarations block
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    AuthModule,
    SharedModule,
    UsersModule,
    ProductsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
// es6 syntax for import and export
// import
// import {} from 'source'
// export export syntax 


// module is a class 
// angular require at least one module (which will act as root module to launch its application)

// 
// angular require at least one module and one component to launch its application


// glossary
// Module
// COmponent
// pipes
// directives
// services
// all these above mentioned terms are class in angular

// selector
// meta data
// decorator
// selector is an term which acts as a html element that carries overall component.
// selector always comes in component

// decorator 
// a decorator is a function which defines class using meta -data
// meta-data ==> meta-data is object which is used to define class
// properties in meta data are defined by decorator
// a decorator always cames in @prefix


// meta data properties of module
// declrations // declration holds componets,pipes,directives of the application
// imports // jahile pani imports vitra module matrai aaucha
// modules can be =
// 1. Inbuilt module(angular io sanga aafai vayeko)
// 2. Thirdpary module (npmjs ma vayeko module)
// 3 user defined module

// providers // this segments holds services
// bootstrap there will be root component for a application which is placed at bootstrap section

// exports all the exported content(component/module)
// entryComponents // modal ma aaune component entry components(they should in declarations)